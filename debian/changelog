swaylock (1.8.0-1) unstable; urgency=medium

  * New upstream version

 -- Birger Schacht <birger@debian.org>  Sat, 24 Aug 2024 15:53:55 +0200

swaylock (1.7.2-1) unstable; urgency=medium

  * New upstream version

 -- Birger Schacht <birger@debian.org>  Mon, 30 Jan 2023 18:32:19 +0100

swaylock (1.7.1-1) unstable; urgency=medium

  * New upstream version
  * Update zsh completion target directory patch
  * Bump standards version
  * Update build dependency name pixbuf2.0 -> pixbuf-2.0
  * Bump years in d/copyright

 -- Birger Schacht <birger@debian.org>  Fri, 27 Jan 2023 12:30:36 +0100

swaylock (1.7-1) unstable; urgency=medium

  * New upstream version
  * Bump standards version

  [ Fabian Grünbichler ]
  * d/control: correct version meson

 -- Birger Schacht <birger@debian.org>  Mon, 28 Nov 2022 08:16:24 +0100

swaylock (1.6-2) unstable; urgency=medium

  * Fix d/watch file

 -- Birger Schacht <birger@debian.org>  Wed, 20 Apr 2022 13:04:49 +0200

swaylock (1.6-1) unstable; urgency=low

  [ Debian Janitor ]
  * Set field Upstream-Name in debian/copyright.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Remove obsolete field Name from debian/upstream/metadata (already present in
    machine-readable debian/copyright).

  [ Birger Schacht ]
  * New upstream release
  * Bump standards version to 4.6.0.1 (no changes required)
  * Update uploader email address
  * Bump year range in d/copyright and update email address
  * Bump debhelper compat version to 13
  * Drup filenamemangle option from d/watch
  * Drop version fix patch

 -- Birger Schacht <birger@debian.org>  Fri, 11 Mar 2022 11:06:10 +0100

swaylock (1.5-2) unstable; urgency=medium

  * Source only upload as-is.

 -- Birger Schacht <birger@rantanplan.org>  Mon, 10 Feb 2020 11:14:16 +0100

swaylock (1.5-1) unstable; urgency=medium

  * New upstream release
  * Added d/gbp.conf file
  * d/control:
    + Updated Vcs-* URLs for new repository
    + Updated Maintainer and Uploaders field
    + Bump Standards-Version to 4.5.0 (no changes required)
  * d/patches:
    + updated 0003-Fix-the-version-in-the-buildfile.patch
    + updated 0001-Workaround-a-bug-in-scdoc.patch

 -- Birger Schacht <birger@rantanplan.org>  Tue, 04 Feb 2020 22:17:12 +0100

swaylock (1.4-1) unstable; urgency=medium

  * New upstream release
  * d/rules: remove override_dh_auto_configure- it was only
    there to set the version and that did not work
  * d/patches: add patch to set the upstream version in the buildfile
  * d/patches: refresh manpage patch
  * d/gitlab-ci.yml: Add a basic gitlab ci configuration
  * d/control: Set versions for build dependencies as specified
    by upstream build file

 -- Birger Schacht <birger@rantanplan.org>  Sat, 18 May 2019 19:00:50 +0200

swaylock (1.3-2) unstable; urgency=medium

  * Add patch to install zsh completion into correct directory

 -- Birger Schacht <birger@rantanplan.org>  Sun, 03 Mar 2019 18:27:15 +0100

swaylock (1.3-1) unstable; urgency=medium

  * Initial packaging (Closes: 921497)

 -- Birger Schacht <birger@rantanplan.org>  Sat, 09 Feb 2019 11:26:23 +0100
